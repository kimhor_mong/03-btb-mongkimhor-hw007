Create react app from scratch

1. run: "npm init -y" or "npm init" then select you own option

2. install dependencies

npm i webpack babel-loader @babel/preset-react babel-preset-react html-webpack-plugin webpack-dev-server css-loader style-loader @babel/plugin-proposal-class-properties webpack-cli -D && npm i react react-dom -S

What we just installed:

- Webpack: bundles all our files into one file
- babel-loader: works with Webpack to transpile ES6+ into ES5 which is supported by older browsers
- @babel/preset-react: extends Babel support to JSX
- html-webpack-plugin: “simplifies the creation of HTML files to serve your Webpack bundles” 
- webpack-dev-server: allows you to use Webpack with a development server that provides live reloading.
- webpack-cli: “webpack CLI provides a flexible set of commands for developers to increase speed when setting up a custom Webpack project.” -https://www.npmjs.com/package/webpack-cli
- css-loader: allows Webpack to convert the CSS file into a JavaScript string.
- style-loader: inserts the JavaScript string into HTML dom.
- @babel/plugin-proposal-class-properties: “This plugin transforms static class properties as well  as properties declared with the property initializer syntax” 
- react: JavaScript library
- react-dom: “Serves as the entry point to the DOM and server renderers for React” -https://www.npmjs.com/package/react-dom

2. add src directory and index.js and App.js

===== code for App.js =====
import React from 'react';

export default function App() {
	return (
		<div>
			<h1>Hello World</h1>
		</div>
	);
}

===== code for index.js =====

import ReactDOM from 'react-dom';
import React from 'react';
import App from './App';

ReactDOM.render(<App />, document.getElementById('app'));


3. add public and create index.html

===== code index.html ======
<!DOCTYPE html>
<html lang="en">
<head>
    <title>My React App</title>
</head>
<body>
    <div id="app"></div>
</body>
</html>

4. add .babelrc &&  webpack.config.js in root directory

Inside of webpack.config.js add this code:

const HtmlWebPackPlugin = require('html-webpack-plugin');
const htmlPlugin = new HtmlWebPackPlugin({
	template: './public/index.html',
	filename: './index.html',
});

const path = require('path');

module.exports = {
	mode: 'development',
	entry: './src/index.js',
	output: {
		path: path.join(__dirname, '/dist'),
		filename: 'index_bundle.js',
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
				},
			},
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader'],
			},
		],
	},
	plugins: [htmlPlugin],
};


5. add run command to package.json, in scripts

"start": "webpack serve --config webpack.config.js",

6. build static asset - option (add build command to package.json, in scripts)
"build": "webpack --mode production --config webpack.config.js",

7. Run react app
open terminal, cd to root directory and run
npm run start

8. build static asset (optional)
npm run build